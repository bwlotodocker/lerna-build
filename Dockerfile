FROM node:16-slim

#RUN apk update && apk upgrade
RUN apt update

#RUN apk --update --no-cache add \
RUN apt install -y \
    python3 \
    python3-pip \
    #py-pip \
    jq \
    bash \
    git \
    groff \
    less \
    #mailcap \
    make \
    bash

RUN pip3 install --no-cache-dir awscli \
    && pip3 install awscli --upgrade --user \
    #&& apk del py-pip \

#RUN rm -rf /var/cache/apk/* /root/.cache/pip/